import requests
import pandas as pd
import numpy as np
from SQL_interface_test_ROB import SQL_Insert
from datetime import datetime

startTime = datetime.now()

# =================================================================
loadPath = 'MLB_player_stats_URL_2006_2017.csv'
# =================================================================

# Web Request Headers
user_agent = 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.3 ' \
             '(KHTML, like Gecko) Chrome/6.0.472.63 Safari/534.3'
headers = {'User-Agent': user_agent}

player_stats_URL_df = pd.read_csv(loadPath)

pitching_columns = ['game_type','gidp','np','opponent_short','sho','bk','game_id','sport_id','sv','bb','whip','avg',
                    'opp_score','team_abbrev','so','tbf','wp','team','league','hb','go_ao','opponent_league',
                    'player_id','cg','team_result','gs','ibb','team_id','home_away','go','hr','game_nbr','irs',
                    'game_score','era','game_day','opponent','ir','g','game_date','sport','l','game_pk','svo',
                    'h','ip','w','s','ao','r','opponent_abbrev','ab','opponent_id','team_score','er']

hitting_columns = ['hr','game_type','game_nbr','player','sac','game_day','rbi','lob','opponent','opponent_short','tb',
                   'game_id','sport_id','bb','slg','avg','opp_score','ops','hbp','d','team_abbrev','so','game_date',
                   'sport','sf','game_pk','team','tpa','league','h','cs','obp','t','ao','r','go_ao','sb',
                   'opponent_abbrev','opponent_league','player_id','ibb','ab','team_result','opponent_id',
                   'team_id','home_away','team_score','go']

pitching_stats_arr = np.array(np.zeros((len(player_stats_URL_df.index), len(pitching_columns))), dtype=np.str)
hitting_stats_arr = np.array(np.zeros((len(player_stats_URL_df.index), len(hitting_columns))), dtype=np.str)

pitching_counter = 0
hitting_counter = 0

for index, row in player_stats_URL_df.iterrows():
    # Prevent repeat download of stats using flag column
    if row['DWNLD'] == 'N':
        r = requests.get(row['URL'], headers=headers)

        if row['Position'] == 'P':
            position = 'pitching'
            game_log_composed = 'sport_pitching_game_log_composed'
            game_log = 'sport_pitching_game_log'
        else:
            position = 'hitting'
            game_log_composed = 'sport_hitting_game_log_composed'
            game_log = 'sport_hitting_game_log'

        stats_json = r.json()
        print(index, row['Player_Name'], row['Position'], row['Year'])
        try:
            for game_num, category in enumerate(stats_json[game_log_composed][game_log]['queryResults']['row']):
                print('Game', game_num)
                column_list, values_list = [], []
                for column in category:
                    column_list.append(column)
                    if column == 'player':
                        # Remove ',' character from name to prevent SQL misread
                        fixed_name = category[column].replace(',', '')
                        values_list.append(fixed_name)
                    else:
                        values_list.append(category[column])
                columns_str = ','.join(column_list)
                values_str = '\'' + '\',\''.join(values_list) + '\''

                # Replace -.-- & .--- with a new identifier to avoid SQL error
                values_str = values_str.replace('.---', 'NO_DATA')
                values_str = values_str.replace('-.--', 'NO_DATA')

                # Save stats to numpy arr
                print(row['Position'])
                if row['Position'] == 'P':
                    pitching_stats_arr[pitching_counter, :] = values_list
                    pitching_counter += 1
                else:
                    hitting_stats_arr[hitting_counter, :] = values_list
                    hitting_counter += 1

            # Writeback to CSV that the stats download and upload to SQL server was successful
            player_stats_URL_df['DWNLD'].ix[index] = 'Y'
            player_stats_URL_df.to_csv('MLB_player_stats_URL_2006_2017.csv', index=False, encoding='utf-8')
        except:
            print('error')

pitching_df = pd.DataFrame(data=pitching_stats_arr, columns=pitching_columns)
hitting_df = pd.DataFrame(data=hitting_stats_arr, columns=hitting_columns)

pitching_df.to_csv('pitching.csv', index=False, encoding='utf-8')
hitting_df.to_csv('hitting.csv', index=False, encoding='utf-8')

# Upload stats to SQL Server
# SQL_Insert(sheet_type=position, columns=columns_str, values=values_str)