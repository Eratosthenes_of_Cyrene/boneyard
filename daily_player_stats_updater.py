# This script should be run daily to update the SQL server with new baseball stats
# Todo: Update the active status of players to the master list as they change over time (Shouldn't affect stats pull)
import requests
import datetime
import pandas as pd
import time
from unidecode import unidecode
from SQL_interface_test_ROB import SQL_Insert, SQL_Insert_All, entryExists, pullPlayerList

# Step #1 - Retrieve a list of the current active players
# Web Request Headers
user_agent = 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.3 ' \
             '(KHTML, like Gecko) Chrome/6.0.472.63 Safari/534.3'
headers = {'User-Agent': user_agent}

# List of team codes for MLB database
teamID_list = [108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 133, 134, 135, 136, 137, 138, 139,
               140, 141, 142, 143, 144, 145, 146, 147, 158]

current_season = datetime.datetime.now().year

active_player_list = []

# Populate list with active players from MLB team rosters
print('====================Step # 1====================')
start_time = time.time()
print('Populating active player list...')
for team in teamID_list:
    active_player_URL = 'http://lookup-service-prod.mlb.com/json/named.roster_team_alltime.bam?' \
                        'roster_team_alltime.col_in=player_html&roster_team_alltime.col_in=player_id&team_id=' + \
                        str(team) + '&start_season=' + str(current_season) + '&end_season=' + str(current_season)

    r = requests.get(active_player_URL, headers=headers)
    json = r.json()

    for player in json['roster_team_alltime']['queryResults']['row']:
        active_player_list.append(player)

active_player_df = pd.DataFrame(data=active_player_list)
print(len(active_player_df), 'active players were found')
print('Complete')
print("--- %s seconds ---" % (time.time() - start_time))
# Check if the player already has an entry in the master player info sheet
print('====================Step # 2====================')
start_time = time.time()
print('Loading master player list...')
player_all_df = pullPlayerList()
# player_all_path = 'MLB_Player_List_All.csv'
# player_all_df = pd.read_csv(player_all_path, encoding='utf-8')
print(len(player_all_df), 'players were found')
print('Complete')

# Use a set (hashtable) to check if players already exist
unique_players = set()

# Add existing players
for player in player_all_df['player_id']:
    unique_players.add(str(player))
print("--- %s seconds ---" %(time.time() - start_time))
# Loop to update the master player list with any new players
print('====================Step # 3====================')
start_time = time.time()
print('Updating master list with any new players...')
num_players_added = 0
for index, new_active_player in active_player_df.iterrows():
    prior_size = len(unique_players)
    unique_players.add(str(new_active_player['player_id']))
    if prior_size != len(unique_players):
        # New player_id was found, pull additional information for this player and add to master dataframe
        player_name = unidecode(new_active_player['player_html'])
        player_info_URL = 'http://lookup-service-prod.mlb.com/json/named.search_player_all.bam?sport_code=%27mlb%27' \
                          '&name_part=%27' + player_name + '%25%27&active_sw=%27Y%27'

        r = requests.get(player_info_URL, headers=headers)
        json = r.json()

        # Check if there are multiple results for the player
        if json['search_player_all']['queryResults']['totalSize'] == '1':
            player_info = pd.Series(json['search_player_all']['queryResults']['row'])
            player_info['service_years'] = current_season
            player_info['name_display_last_first'].replace(',', '')

            columns_list, values_list = [], []
            for column, value in player_info.iteritems():
                columns_list.append(str(column))
                values_list.append(str(value).replace('\'', ''))
            columns_str = ','.join(columns_list)
            values_str = '\'' + '\',\''.join(values_list) + '\''
        else:
            # If there are multiple results then match the player_id
            for player in json['search_player_all']['queryResults']['row']:
                if player['player_id'] == new_active_player['player_id']:
                    player_info = pd.Series(player)
                    player_info['service_years'] = current_season
                    player_info['name_display_last_first'].replace(',', '')

                    columns_list, values_list = [], []
                    for column, value in player_info.iteritems():
                        columns_list.append(str(column))
                        values_list.append(str(value).replace('\'', ''))
                    columns_str = ','.join(columns_list)
                    values_str = '\'' + '\',\''.join(values_list) + '\''

        SQL_Insert(sheet_type='MLB_Player_list', columns=columns_str, values=values_str)

        # Add to all player list
        print(player_info['name_display_first_last'], 'was added to the master player list')
        player_all_df = player_all_df.append(player_info, ignore_index=True)
        num_players_added += 1
print(num_players_added, 'new players were added')

# Write-back the updated Dataframe
player_all_df.to_csv('MLB_Player_List_All.csv', index=False, encoding='utf-8')
print('Complete')
print("--- %s seconds ---" % (time.time() - start_time))
# Step #2 - Pull actual gamelogs for each player on the active list
print('====================Step # 4====================')
print('Pulling gamelogs for active players...')
for index, player in active_player_df.iterrows():
    all_player_row = player_all_df.loc[player_all_df['player_id'] == player['player_id']]

    if all_player_row['position'].values == 'P':
        position = 'pitching'
        game_log_composed = 'sport_pitching_game_log_composed'
        game_log = 'sport_pitching_game_log'
    else:
        position = 'hitting'
        game_log_composed = 'sport_hitting_game_log_composed'
        game_log = 'sport_hitting_game_log'

    # Build Stats URL
    player_stats_URL = 'http://lookup-service-prod.mlb.com/json/named.sport_' + position + \
                       '_game_log_composed.bam?game_type=%27R%27&league_list_id=%27mlb_hist%27&player_id=' + \
                       str(player['player_id']) + '&season=' + str(current_season) + '&sit_code=%271%27' \
                       '&sit_code=%272%27&sit_code=%273%27&sit_code=%274%27&sit_code=%275%27&sit_code=%276%27' \
                       '&sit_code=%277%27&sit_code=%278%27&sit_code=%279%27&sit_code=%2710%27&sit_code=%2711%27' \
                       '&sit_code=%2712%27'
    print(player_stats_URL)
    r = requests.get(player_stats_URL, headers=headers)
    json = r.json()

    # Pull Gamelogs and Form SQL String
    values_list_multigame = []
    column_list = []

    # Single Game for Season
    if json[game_log_composed][game_log]['queryResults']['totalSize'] == '1':
        values_list = []
        game = json[game_log_composed][game_log]['queryResults']['row']
        for column in game:
            column_list.append(column)
            if column == 'player':
                # Remove ',' character from name to prevent SQL misread
                fixed_name = game[column].replace(',', '')
                values_list.append(fixed_name)
            else:
                values_list.append(game[column])

        # Only make the column string one time
        columns_str = ','.join(column_list)

        values_str = '\'' + '\',\''.join(values_list) + '\''
        # Replace -.-- & .--- with a new identifier to avoid SQL error
        values_str = values_str.replace('.---', 'NO_DATA')
        values_str = values_str.replace('-.--', 'NO_DATA')

        # Add values string to the multi-game list
        if entryExists(player_id=game['player_id'], game_id=game['game_id'], sheet_type=position):
            pass
        else:
            values_list_multigame.append(values_str)
    # Multiple Games for Season
    else:
        start_time = time.time()
        for i, game in enumerate(json[game_log_composed][game_log]['queryResults']['row']):
            values_list = []
            for column in game:
                column_list.append(column)
                if column == 'player':
                    # Remove ',' character from name to prevent SQL misread
                    fixed_name = game[column].replace(',', '')
                    values_list.append(fixed_name)
                else:
                    values_list.append(game[column])
            # Only make the column string one time
            if i == 0:
                columns_str = ','.join(column_list)

            values_str = '\'' + '\',\''.join(values_list) + '\''
            # Replace -.-- & .--- with a new identifier to avoid SQL error
            values_str = values_str.replace('.---', 'NO_DATA')
            values_str = values_str.replace('-.--', 'NO_DATA')

            # Add values string to the multi-game list
            if entryExists(player_id=game['player_id'], game_id=game['game_pk'], sheet_type=position):
                pass
            # else:
            #     values_list_multigame.append(values_str)

    # Upload stats to SQL Server
    if len(values_list_multigame) != 0:
        SQL_Insert_All(sheet_type=position, columns=columns_str, values_list=values_list_multigame)
    print(str(len(values_list_multigame)) + '/' + json[game_log_composed][game_log]['queryResults']['totalSize'],
          'games added for', player['player_html'], 'in', current_season)
    print("--- %s seconds ---" %(time.time() - start_time))

print('Complete')
print('============================================')
