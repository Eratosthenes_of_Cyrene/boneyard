# Todo: Change SQL variable types to all strings
# Todo: Remove/Adjust columns with data '---'
import pyodbc
import pandas as pd

def SQL_Insert(sheet_type, columns, values, server='retrosheets.database.windows.net', database='baseball_b', uid='mattymonation',
              pwd='Fuckums123'):

    cnxn = pyodbc.connect("Driver={SQL Server Native Client 11.0};"
                          "Server=" + server + ";"
                          "Database=" + database + ";"
                          "uid=" + uid + ";pwd=" + pwd)
    cursor = cnxn.cursor()
    insert_cmd = "insert into dbo." + sheet_type + "(" + columns + ") values (" + values + ")"
    # print(insert_cmd)
    cursor.execute(insert_cmd)
    cnxn.commit()

def SQL_Insert_All(sheet_type, columns, values_list, server='retrosheets.database.windows.net', database='baseball_b',
               uid='mattymonation', pwd='Fuckums123'):
    cnxn = pyodbc.connect("Driver={SQL Server Native Client 11.0};"
                          "Server=" + server + ";"
                          "Database=" + database + ";"
                          "uid=" + uid + ";pwd=" + pwd)
    cursor = cnxn.cursor()
    insert_cmd_front = "insert into dbo.stats_game_" + sheet_type + "(" + columns + ") values "
    insert_cmd_back = None
    for i, values in enumerate(values_list):
        if i == 0:
            insert_cmd_back = "(" + values + ")" + ','
        else:
            insert_cmd_back += "(" + values + ")" + ','
    # Remove Extra comma at end of string
    insert_cmd_back = insert_cmd_back[:-1]
    insert_cmd = insert_cmd_front + insert_cmd_back
    # print(insert_cmd)
    cursor.execute(insert_cmd)
    cnxn.commit()

def entryExists(player_id, game_id, sheet_type, server='retrosheets.database.windows.net', database ='baseball_b',
                uid='rob', pwd='1qaz2wsx#EDC'):
    cnxn = pyodbc.connect("Driver={SQL Server Native Client 11.0};"
                          "Server=" + server + ";"
                          "Database=" + database + ";"
                          "uid=" + uid + ";pwd=" + pwd)
    cursor = cnxn.cursor()
    # select_cmd = "select * from dbo.stats_game_" + sheet_type + " where player_id = " + str(player_id) + \
    #              " AND game_id = \'" + game_id + "\'"
    select_cmd = "select unique_id from dbo.onetenthtest where unique_id = " + str(player_id) + str(game_id)
    cursor.execute(select_cmd)
    result = cursor.fetchall()
    if len(result) == 0:
        # No entry exists
        return False
    else:
        # One or more entries exist
        return True

def SQL_Select(sheet_type, column, data, server='retrosheets.database.windows.net', database='baseball_b', uid='rob',
              pwd='1qaz2wsx#EDC'):

    cnxn = pyodbc.connect("Driver={SQL Server Native Client 11.0};"
                          "Server=" + server + ";"
                          "Database=" + database + ";"
                          "uid=" + uid + ";pwd=" + pwd)
    df = pd.read_sql_query('select top(1000) * from dbo.players', cnxn)
    return df

def SQL_Delete(sheet_type, column, data, server='retrosheets.database.windows.net', database='baseball_b', uid='rob',
              pwd='1qaz2wsx#EDC'):
    cnxn = pyodbc.connect("Driver={SQL Server Native Client 11.0};"
                          "Server=" + server + ";" 
                          "Database=" + database + ";" 
                          "uid=" + uid + ";pwd=" + pwd)
    return None

def SQL_Update(sheet_type, column, data, server='retrosheets.database.windows.net', database='baseball_b', uid='rob',
               pwd='1qaz2wsx#EDC'):
    cnxn = pyodbc.connect("Driver={SQL Server Native Client 11.0};"
                          "Server=" + server + ";"
                          "Database=" + database + ";"
                          "uid=" + uid + ";pwd=" + pwd)
    return None

def pullPlayerList(server='retrosheets.database.windows.net', database='baseball_b', uid='rob',
                   pwd='1qaz2wsx#EDC'):
    cnxn = pyodbc.connect("Driver={SQL Server Native Client 11.0};"
                          "Server=" + server + ";"
                          "Database=" + database + ";"
                          "uid=" + uid + ";pwd=" + pwd)

    sql = "select * from dbo.MLB_Player_List where active_sw = 'y'"

    df = pd.read_sql_query(sql, cnxn)
    cnxn.close()

    return df