import requests
import datetime
from extract_data import extractData

def checkGames(url):
    user_agent = 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.3 (KHTML, like Gecko) Chrome/6.0.472.63 Safari/534.3'
    headers = {'User-Agent': user_agent}
    r = requests.get(url, headers=headers)
    if len(r.text) > 300000:
        return True
    else:
        return False

baseURL = 'http://www.espn.com/mlb/scoreboard/_/date/'
current_date = str(datetime.datetime.now().date()).replace('-', '')
todayURL = baseURL + current_date

if checkGames(todayURL):
    # Web Request Headers
    user_agent = 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.3 (KHTML, like Gecko) Chrome/6.0.472.63 Safari/534.3'
    headers = {'User-Agent': user_agent}

    # Grab html from redirect page for comparison later
    refURL = 'http://www.espn.com/mlb/scoreboard'
    refHTML = requests.get(refURL, headers=headers)

    # Proceed to pull box scores from each game
    for game_num in range(1, 31):
        # Form box score url for game
        gameURL = 'http://www.espn.com/mlb/boxscore?gameId=3' + str(current_date[3:]) + '1' + str(game_num).zfill(2)
        # Make request to URL
        r = requests.get(gameURL, headers=headers)
        # Check if game data exists by comparing to ref page
        if len(r.text) != len(refHTML.text):
            # Data Exists
            print('Data', game_num, len(refHTML.text), len(r.text), gameURL)
            box_score, away_hitting, away_pitching, home_hitting, home_pitching = extractData(r.text)
            print(box_score)
        else:
            # Data Does Not Exist
            print('No Data', game_num, len(refHTML.text), len(r.text), gameURL)
else:
    print('No games on', current_date)
