import numpy as np
import pandas as pd

df = pd.read_csv('MLB_player_stats_URL.csv')
df['DWNLD'] = 'N'
df_arr = df.values
df_sub_arr = np.array_split(df_arr, 3)

for i, sub_arr in enumerate(df_sub_arr):
    sub_df = pd.DataFrame(data=sub_arr, columns=df.columns)
    sub_df.to_csv('Player_Stats_CSVs/MLB_player_stats_URL_' + str(i) + '.csv', index=False, encoding='utf-8')
