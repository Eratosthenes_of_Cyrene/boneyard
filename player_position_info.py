import requests
import pandas as pd

# Load Dataframe
df = pd.read_csv('MLB_player_List_2006_2017.csv')

# Web Request Headers
user_agent = 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.3 ' \
             '(KHTML, like Gecko) Chrome/6.0.472.63 Safari/534.3'
headers = {'User-Agent': user_agent}

num_positions = 9
player_position_list = []
for x in range(1, num_positions + 1):
    player_position_URL = 'http://lookup-service-prod.mlb.com/json/named.search_player_all_pos.bam?sport_code=%27mlb%27' \
                          '&active_sw=%27Y%27&position=%27' + str(x) + '%27'
    print(player_position_URL)
    r = requests.get(player_position_URL, headers=headers)
    stats_json = r.json()

    for item in stats_json['search_player_all_pos']['queryResults']['row']:
        player_position_list.append((item['player_id'], item['position']))
        print(item['player_id'], item['position'])

position_df = pd.DataFrame(data=player_position_list, columns=['Player_ID', 'Position'])

merged_union_df = pd.concat([df, position_df], axis=1, join='outer')
merged_intersect_df = pd.concat([df, position_df], axis=1, join='inner')
merged_union_df.to_csv('MLB_Player_List_Pos_All_2006_2017.csv', index=False, encoding='utf-8')
merged_intersect_df.to_csv('MLB_Player_List_Pos_Reduced_2006_2017.csv', index=False, encoding='utf-8')
