# Todo: Grab Minor League Stats As Well (-minor in the gamelogs URL)
# Todo: Some players may not have played in a year, but were not retired
# Todo: Fielding might not work (can't pull stats)
# Todo: Sometimes there are connection errors to website which stops the code
import requests
import pandas as pd
import numpy as np
import os
import scrape_table
import datetime
import unidecode
from extract_data import extractData
from bs4 import BeautifulSoup

# Web Request Headers
user_agent = 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.3 ' \
             '(KHTML, like Gecko) Chrome/6.0.472.63 Safari/534.3'
headers = {'User-Agent': user_agent}

teamID_list = [108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 133, 134, 135, 136, 137, 138, 139,
               140, 141, 142, 143, 144, 145, 146, 147, 158]
team_start_year = 2006
team_end_year = 2017

MLB_player_set = set()
raw_player_count = 0

# Obtain a Unique Set of MLB Players
for teamID in teamID_list:
    # Build Query URL for Each Team using TeamID and Year Range
    queryURL = 'http://lookup-service-prod.mlb.com/json/named.roster_team_alltime.bam?' \
               'roster_team_alltime.col_in=player_html&roster_team_alltime.col_in=player_id&team_id=' + str(teamID) + \
               '&start_season=' + str(team_start_year) + '&end_season=' + str(team_end_year) + '&ovrd_enc=utf-8'
    r = requests.get(queryURL, headers=headers).json()

    for item in r['roster_team_alltime']['queryResults']['row']:
        player_name = item['player_html'].lower()
        player_ID = item['player_id']
        MLB_player_set.add((player_name, player_ID))

# Convert Unique Players From Set to List & Save as CSV
MLB_player_list = sorted(MLB_player_set)
print(len(MLB_player_list), 'Unique Players Found')

# Create CSV if it doesn't already exist
if not os.path.exists('MLB_Player_List_' + str(team_start_year) + '_' + str(team_end_year) + '.csv'):
    print('New MLB player CSV created')
    MLB_player_df = pd.DataFrame(data=MLB_player_list, columns=['Player_Name', 'Player_ID'])
    # Create New Columns for Year Information
    MLB_player_df['Start_Year'] = pd.Series(np.zeros(len(MLB_player_df.index)), index=MLB_player_df.index)
    MLB_player_df['End_Year'] = pd.Series(np.zeros(len(MLB_player_df.index)), index=MLB_player_df.index)
    MLB_player_df.to_csv('MLB_Player_List_' + str(team_start_year) + '_' + str(team_end_year) + '.csv', index=False,
                     encoding='utf-8')

# Base URL for Pulling Active Player Years
baseURL = 'http://m.mlb.com/player/'

#  Check for a partially complete CSV File and start from last player
MLB_player_df = pd.read_csv('MLB_Player_List_2006_2017.csv')

# Offset for header row
start_index = 0

for index, row in MLB_player_df.iterrows():
    if MLB_player_df['Start_Year'].ix[index] == 0.0:
        break
    else:
        start_index += 1

# Remove Bad Player (Didn't actually play)
MLB_player_list.remove(('weiland, kyle', '475095'))

# Pull Player Stats (Game by Game)
for i, player in enumerate(MLB_player_list, start=start_index):
    print(i, '/', len(MLB_player_list))
    print(MLB_player_list[i])

    full_name = MLB_player_list[i][0]
    full_name = unidecode.unidecode(full_name)
    full_name = full_name.replace('.', '')
    full_name = full_name.replace('\'', '')

    last_name = full_name.split(',')[0]
    first_name = full_name.split(',')[1][1:]
    player_ID = MLB_player_list[i][1]

    playerURL = str('/').join([baseURL, player_ID, first_name + '-' + last_name])
    playerURL = playerURL.replace(' ', '-')
    print(playerURL)

    # Determine Seasons for this Player
    r = requests.get(playerURL, headers=headers)

    # First Season
    debut_start_index = r.text.find('Debut:')
    player_start_year = r.text[debut_start_index + 18:debut_start_index + 23]
    player_start_year = player_start_year.replace('<', '')
    player_start_year = player_start_year[-4:]
    player_start_year = int(player_start_year)
    MLB_player_df['Start_Year'].ix[i] = player_start_year

    #  Last Season (Exclude Current Season)
    LG_start_index = r.text.find('Last Game:')
    player_end_year = r.text[LG_start_index + 17:LG_start_index + 27]
    if player_end_year == '<!--[if lt':
        current_date = str(datetime.datetime.now().date()).replace('-', '')
        player_end_year = int(current_date[:4])
    else:
        player_end_year = player_end_year[-5:]
        player_end_year = player_end_year.replace('/', '')
        player_end_year = player_end_year.replace('\n', '')
        player_end_year = int(player_end_year)
    MLB_player_df['End_Year'].ix[i] = player_end_year
    print(player_start_year, player_end_year)

    # CSV Save Interval
    if i % 1 == 0:
        MLB_player_df.to_csv('MLB_Player_List_' + str(team_start_year) + '_' + str(team_end_year) + '.csv',
                             index=False, encoding='utf-8')
        print('Progress Saved to CSV')
