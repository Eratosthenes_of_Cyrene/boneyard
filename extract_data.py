import pandas as pd
import numpy as np
import scrape_table
from bs4 import BeautifulSoup

def extractData(html):
    # Parse Tables from Box Score HTML
    # Table 0 = Team Names, Inning Scoring, and Total Runs
    # Table 1 = Away Team Hitting
    # Table 3 = Away Team Pitching
    # Table 5 = Home Team Hitting
    # Table 7 = Home Team Pitching

    soup = BeautifulSoup(html, 'lxml')
    tables = soup.find_all('table')

    # Extract Table Data as Dataframes
    hp = scrape_table.HTMLTableParser()
    boxscore_df = hp.parse_html_table(tables[0])
    away_hitting_df = hp.parse_html_table(tables[1])
    away_pitching_df = hp.parse_html_table(tables[3])
    home_hitting_df = hp.parse_html_table(tables[5])
    home_pitching_df = hp.parse_html_table(tables[7])

    # Clean-up and Re-Format the Generated Dataframes
    # Box Score
    new_cols = ['Team', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'R', 'H', 'E']
    boxscore_df = pd.DataFrame(data=np.array(boxscore_df.iloc[1:]), columns=new_cols)
    for i, team_name in enumerate(boxscore_df['Team']):
        temp_name = team_name[5:-2]
        temp_name = temp_name.split('\n')
        new_team_name = temp_name[0] + '-' + temp_name[1]
        boxscore_df.set_value(i, 'Team', new_team_name)

    return boxscore_df, away_hitting_df, away_pitching_df, home_hitting_df, home_pitching_df
